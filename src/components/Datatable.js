import { TableContainer, Table, TableHead, TableRow, TableCell, TableBody, Pagination, Grid, Button, Typography, ButtonGroup, Select, MenuItem, FormControl, InputLabel } from "@mui/material"
import { Container } from "@mui/system";
import { useEffect, useState } from "react"
import * as React from 'react';
import ModalAdd from "./modalAdd";
import { fetchAPI } from "./FetchAPI";
import ModalEdit from "./modalEdit";
import ModalDelete from "./modalDelete";

function Datatable() {
    const [limit, setLimit] = useState(10);
    const [rows, setRow] = useState([]);
    const [page, setPage] = useState(1);
    const [noPage, setNoPage] = useState(0);

    //Insert
    const [open, setOpen] = React.useState(false);
    const handleClose = () => { setOpen(false) }

    //Sửa
    const [openModalEdit, setOpenModalEdit] = useState(false)
    const [rowEdit, setRowEdit] = useState({ "id": 572141, "orderId": "qPWmY4d5PF", "kichCo": "M", "duongKinh": "25", "suon": 4, "salad": "300", "loaiPizza": "Hawaii", "idVourcher": "12332", "thanhTien": 200000, "giamGia": 20000, "idLoaiNuocUong": "TRASUA", "soLuongNuoc": 3, "hoTen": "chi", "email": "chi@gmail.com", "soDienThoai": "0123456789", "diaChi": "asdfghj", "loiNhan": "", "trangThai": "open", "ngayTao": 1657349980052, "ngayCapNhat": 1657349980052 })
    const handleCloseEdit = () => { setOpenModalEdit(false) }

    //Xóa
    const [openModalDelete, setOpenModalDelete] = useState(false)
    const [rowDelete, setRowDelete] = useState({ id: 0 })
    const handleCloseDelete = () => { setOpenModalDelete(false) }


    const changeLimitPage = (event) => {
        setLimit(event.target.value)
    }

    const changeHandler = (event, value) => {
        setPage(value)
    }


    //Insert
    const addButton = () => {
        setOpen(true);
    }
    //Edit
    const editButtonHandler = (row) => {
        setRowEdit(row);
        setOpenModalEdit(true);
    }

    //Delete
    const deleteButtonHandler = (row) => {
        setOpenModalDelete(true)
        setRowDelete(row);
    }


    useEffect(() => {
        fetchAPI("http://42.115.221.44:8080/devcamp-pizza365/orders/")
            .then((data) => {
                setNoPage(Math.ceil(data.length / limit));

                setRow(data.slice((page - 1) * limit, page * limit));
            })
            .catch((error) => {
                console.error(error.message)
            })
    }, [page, limit, open, openModalEdit, openModalDelete])

    return (
        <Container fixed>

            {/* Modal */}
            {/* Thêm */}
            <ModalAdd openProps={open} closeProps={handleClose}></ModalAdd>

            {/* Sửa */}
            <ModalEdit openProps={openModalEdit} closeProps={handleCloseEdit} idEdit={rowEdit}></ModalEdit>

            {/* Xóa */}
            <ModalDelete openProps={openModalDelete} closeProps={handleCloseDelete} idDelete={rowDelete.id}></ModalDelete>

            <Typography variant="h3" align="center">Danh sách đơn hàng</Typography >

            <Grid container>
                <Grid item>
                    <FormControl fullWidth sx={{ minWidth: 250 }}>
                        <InputLabel>NoPage</InputLabel>
                        <Select label="NoPage" onChange={changeLimitPage} defaultValue={10}>
                            <MenuItem value={5}>5</MenuItem>
                            <MenuItem value={10}>10</MenuItem>
                            <MenuItem value={25}>25</MenuItem>
                            <MenuItem value={50}>50</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
            </Grid>
            <Grid container sx={{ pt: 2 }}>
                <Grid item>
                    <Button variant="contained" color="success" onClick={addButton}>Thêm</Button>
                </Grid>
            </Grid>

            <Grid container>
                <Grid item>
                    <TableContainer>
                        <Table sx={{ width: "1300px" }} aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell align="center" sx={{ fontWeight: "bold" }}>Id</TableCell>
                                    <TableCell align="center" sx={{ fontWeight: "bold" }}>Order Id</TableCell>
                                    <TableCell align="center" sx={{ fontWeight: "bold" }}>Kích cỡ combo</TableCell>
                                    <TableCell align="center" sx={{ fontWeight: "bold" }}>Loại pizza</TableCell>
                                    <TableCell align="center" sx={{ fontWeight: "bold" }}>Nước uống</TableCell>
                                    <TableCell align="center" sx={{ fontWeight: "bold" }}>Thành tiền</TableCell>
                                    <TableCell align="center" sx={{ fontWeight: "bold" }}>Họ và tên</TableCell>
                                    <TableCell align="center" sx={{ fontWeight: "bold" }}>Số điện thoại</TableCell>
                                    <TableCell align="center" sx={{ fontWeight: "bold" }}>Trạng thái</TableCell>
                                    <TableCell align="center" sx={{ fontWeight: "bold" }}>Action</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    rows.map((row, index) => {
                                        return (
                                            <TableRow key={index}>
                                                <TableCell align="center">{row.id}</TableCell>
                                                <TableCell align="center">{row.orderId}</TableCell>
                                                <TableCell align="center">{row.kichCo}</TableCell>
                                                <TableCell align="center">{row.loaiPizza}</TableCell>
                                                <TableCell align="center">{row.idLoaiNuocUong}</TableCell>
                                                <TableCell align="center">{row.thanhTien}</TableCell>
                                                <TableCell align="center">{row.hoTen}</TableCell>
                                                <TableCell align="center">{row.soDienThoai}</TableCell>
                                                <TableCell align="center">{row.trangThai}</TableCell>
                                                <TableCell align="center">
                                                    <ButtonGroup variant="contained" aria-label="outlined primary button group">
                                                        <Button onClick={() => { editButtonHandler(row) }}>Sửa</Button>
                                                        <Button color="error" onClick={() => { deleteButtonHandler(row) }}>Xóa</Button>
                                                    </ButtonGroup>
                                                </TableCell>
                                            </TableRow>
                                        )
                                    })
                                }
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
            </Grid>
            <Grid container mt={3} mb={3} justifyContent="flex-end">
                <Grid item>
                    <Pagination count={noPage} defaultPage={page} onChange={changeHandler}></Pagination>
                </Grid>
            </Grid>
        </Container >

    )
}
export default Datatable