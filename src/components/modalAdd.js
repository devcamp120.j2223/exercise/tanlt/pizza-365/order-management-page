import { Input, Grid, Typography, Button, Alert, Snackbar, Modal, Box, Select, MenuItem } from '@mui/material';
import { useEffect, useState } from 'react';
import { fetchAPI } from './FetchAPI';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: "500px",
    height: "600px",
    overflow: 'scroll',
    bgcolor: 'background.paper',
    border: '2px solid #000',
    p: 4,
};

function ModalAdd({ openProps, closeProps }) {
    const [drinkList, setDrinkList] = useState([])
    const [fullName, setFullname] = useState("");
    const [phone, setPhone] = useState("");
    const [email, setEmail] = useState("");
    const [diachi, setDiachi] = useState("");
    const [voucher, setVoucher] = useState("");
    const [message, setMessage] = useState("");
    const [submit, setSubmit] = useState(false);
    const [checkEmail, setCheckEmail] = useState(true);
    const [checkPhone, setCheckPhone] = useState(true);
    const [giamGia, setGiamGia] = useState(0);
    const [combo, setCombo] = useState(0);
    const [drink, setDrink] = useState(0);
    const [pizza, setPizza] = useState(0);
    const [size, setSize] = useState({})
    const [niceCombo, setNiceCombo] = useState(0)
    const [openAlert, setOpenAlert] = useState(false);
    const handleClose = () => { setOpenAlert(false) }
    const [success, setSuccess] = useState(false);
    const [fail, setFail] = useState(false);
    const [alertMessage, setAlertMessage] = useState("")
    const [noti, setNoti] = useState(true)

    const onBtnGui = () => {
        setSubmit(true);
        setNoti(validateData())
        if (validateData()) {
            taoDonHang();
        }
    }
    const validateData = () => {
        if (fullName === "") {
            return false
        }
        if (email.indexOf("@") === -1) {
            setCheckEmail(false)
            return false
        } else { setCheckEmail(true) }
        if (isNaN(phone)) {
            setCheckPhone(false);
            return false
        }
        if (phone === "") {
            setCheckPhone(false);
            return false
        } else { setCheckPhone(true) }
        if (diachi === "") {
            return false
        }
        if (!combo) {
            return false
        }
        if (!drink) {
            return false
        }
        if (!pizza) {
            return false
        }
        return true;
    }

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    const taoDonHang = () => {
        let body = {
            method: 'POST',
            body: JSON.stringify({
                kichCo: combo,
                duongKinh: size.duongKinhCM,
                suon: size.suonNuong,
                salad: size.saladGr,
                loaiPizza: pizza,
                idVourcher: voucher,
                idLoaiNuocUong: drink,
                soLuongNuoc: size.drink,
                hoTen: fullName,
                thanhTien: size.priceVND,
                email: email,
                soDienThoai: phone,
                diaChi: diachi,
                loiNhan: message
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        };
        fetchAPI("http://42.115.221.44:8080/devcamp-pizza365/orders", body)
            .then((data) => {
                setSuccess(true)
                setAlertMessage("Thêm thành công");
                closeProps();
            })
            .catch((err) => {
                setFail(true);
                setAlertMessage("Thêm thất bại")
            })
    }

    useEffect(() => {
        const kiemTraVoucher = () => {
            if (voucher) {
                fetchAPI("http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/" + voucher)
                    .then((data) => {
                        setGiamGia(data.phanTramGiamGia);
                        setOpenAlert(false)
                    })
                    .catch((err) => {
                        setOpenAlert(true)
                        setGiamGia(0);
                    })
            }
            else { setGiamGia(0); setOpenAlert(false) }
        }
        fetchAPI("http://42.115.221.44:8080/devcamp-pizza365/drinks")
            .then((data) => {
                setDrinkList(data);
            })
        kiemTraVoucher();
        if (combo === "S") {
            setSize({
                menuName: "S",    // S, M, L
                duongKinhCM: "20",
                suonNuong: 2,
                saladGr: 200,
                drink: 2,
                priceVND: 150000
            })
            setNiceCombo(1)
        }
        if (combo === "M") {
            setSize({
                menuName: "M",    // S, M, L
                duongKinhCM: "25",
                suonNuong: 4,
                saladGr: 300,
                drink: 3,
                priceVND: 200000
            })
            setNiceCombo(1)
        }
        if (combo === "L") {
            setSize({
                menuName: "L",    // S, M, L
                duongKinhCM: "30",
                suonNuong: 8,
                saladGr: 500,
                drink: 4,
                priceVND: 250000
            })
            setNiceCombo(1)
        }
    }, [voucher, combo])
    return (
        <Grid>
            <Modal open={openProps} aria-labelledby="modal-modal-title" aria-describedby="modal-modal-description">
                <Box sx={style}>
                    {/* <!-- Title Gửi đơn hàng --> */}
                    <Grid sm={12} textAlign="center">
                        <Typography variant='h4'>Thêm đơn hàng</Typography>
                    </Grid>
                    {
                        openAlert ?
                            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleClose}>
                                <Alert onClose={handleClose} severity="error" sx={{ width: '100%' }}>
                                    Mã giảm giá không tồn tại
                                </Alert>
                            </Snackbar> : null
                    }
                    {/* <!-- Content Gửi đơn hàng --> */}
                    <Grid sm={12}>

                        <Grid item>
                            <Typography htmlFor="fullname">Tên</Typography>
                            <Input sx={{ borderRadius: "5px", padding: 0.5 }} value={fullName} id="inp-fullname" placeholder="Nhập tên" fullWidth onChange={(event) => { setFullname(event.target.value) }} />
                            {
                                submit
                                    ? (fullName ? null : <Typography color="error">Nhập họ và tên</Typography>)
                                    : null

                            }
                        </Grid>
                        <Grid item mt={3}>
                            <Typography htmlFor="email">Email</Typography>
                            <Input sx={{ borderRadius: "5px", padding: 0.5 }} value={email} id="inp-email" placeholder="Nhập email" fullWidth onChange={(event) => { setEmail(event.target.value) }} />
                            {
                                submit
                                    ? (checkEmail ? null : <Typography color="error">Email không hợp lệ</Typography>)
                                    : null

                            }
                        </Grid>
                        <Grid item mt={3}>
                            <Typography htmlFor="dien-thoai">Số điện thoại</Typography>
                            <Input sx={{ borderRadius: "5px", padding: 0.5 }} value={phone} id="inp-dien-thoai" placeholder="Nhập số điện thoại" fullWidth onChange={(event) => { setPhone(event.target.value) }} />
                            {
                                submit
                                    ? (checkPhone ? null : <Typography color="error">Nhập số điện thoại</Typography>)
                                    : null

                            }
                        </Grid>
                        <Grid item mt={3}>
                            <Typography htmlFor="dia-chi">Địa chỉ</Typography>
                            <Input sx={{ borderRadius: "5px", padding: 0.5 }} value={diachi} id="inp-dia-chi" placeholder="Địa chỉ" fullWidth onChange={(event) => { setDiachi(event.target.value) }} />
                            {
                                submit
                                    ? (diachi ? null : <Typography color="error">Nhập địa chỉ</Typography>)
                                    : null

                            }
                        </Grid>
                        <Grid item mt={3}>
                            <Typography htmlFor="voucher">Mã giảm giá</Typography>
                            <Input sx={{ borderRadius: "5px", padding: 0.5 }} value={voucher} id="inp-voucher" placeholder="Nhập mã giảm giá" fullWidth onChange={(event) => { setVoucher(event.target.value) }} />
                        </Grid>
                        <Grid item mt={3} mb={3}>
                            <Typography htmlFor="message">Lời nhắn</Typography>
                            <Input sx={{ borderRadius: "5px", padding: 0.5 }} value={message} id="inp-message" placeholder="Nhập lời nhắn" fullWidth onChange={(event) => { setMessage(event.target.value) }} />
                        </Grid>
                        <Grid item mt={3} mb={3}>
                            <Typography htmlFor="message">Chọn combo</Typography>
                            <Select label="combo" defaultValue={0} onChange={(event) => { setCombo(event.target.value) }} fullWidth>
                                <MenuItem value={0}>Chọn combo</MenuItem>
                                <MenuItem value="S">Small</MenuItem>
                                <MenuItem value="M">Medium</MenuItem>
                                <MenuItem value="L">Large</MenuItem>
                            </Select>
                            {
                                niceCombo ? combo ?
                                    <Grid>
                                        <Typography htmlFor="message" paddingTop={2}>Đường kính: {size.duongKinhCM}cm</Typography>
                                        <Typography htmlFor="message" paddingTop={2}>Sườn nướng: {size.suonNuong}</Typography>
                                        <Typography htmlFor="message" paddingTop={2}>Salad: {size.saladGr}g</Typography>
                                        <Typography htmlFor="message" paddingTop={2}>Số lượng nước: {size.drink}</Typography>
                                        <Typography variant='h6' htmlFor="message" paddingTop={2}>Đơn giá: {numberWithCommas(size.priceVND)} VNĐ</Typography>
                                    </Grid>
                                    : null : null
                            }
                            {
                                submit
                                    ? (combo ? null : <Typography color="error">Hãy chọn combo</Typography>)
                                    : null

                            }
                        </Grid>
                        <Grid item mt={3} mb={3}>
                            <Typography htmlFor="message">Loại nước uống</Typography>
                            <Select label="drink" defaultValue={0} onChange={(event) => { setDrink(event.target.value) }} fullWidth>
                                <MenuItem value={0}>Tất cả nước uống</MenuItem>
                                {
                                    drinkList.map((vDrink, index) => {
                                        return <MenuItem key={index} value={vDrink.maNuocUong}>{vDrink.tenNuocUong}</MenuItem>
                                    })
                                }
                            </Select>
                            {
                                submit
                                    ? (drink ? null : <Typography color="error">Hãy chọn nước uống</Typography>)
                                    : null

                            }
                        </Grid>
                        <Grid item mt={3} mb={3}>
                            <Typography htmlFor="message">Loại Pizza</Typography>
                            <Select label="pizza" defaultValue={0} onChange={(event) => { setPizza(event.target.value) }} fullWidth>
                                <MenuItem value={0}>Chọn loại pizza</MenuItem>
                                <MenuItem value="Seafood">Hải Sản</MenuItem>
                                <MenuItem value="Hawaii">Hawaii</MenuItem>
                                <MenuItem value="Bacon">Thịt Hun Khói</MenuItem>
                            </Select>
                            {
                                submit
                                    ? (pizza ? null : <Typography color="error">Hãy chọn loại pizza</Typography>)
                                    : null

                            }
                        </Grid>
                        <Grid item mt={3} mb={3}>
                            <Typography htmlFor="message">Trạng thái</Typography>
                            <Select label="status" defaultValue="open" fullWidth>
                                <MenuItem value="open">Open</MenuItem>
                                <MenuItem value="cancel">Cancel</MenuItem>
                                <MenuItem value="confirmed">Confirmed</MenuItem>
                            </Select>
                        </Grid>
                        {
                            size.priceVND ? combo ?
                                <Grid>
                                    {giamGia ? <Typography htmlFor="message">Giảm giá: {numberWithCommas(size.priceVND * giamGia / 100)} VNĐ ( {giamGia} %)</Typography> : null}
                                    <Typography variant='h6' htmlFor="message" padding={2}>Thành tiền: {numberWithCommas(size.priceVND - (size.priceVND * giamGia) / 100)} VNĐ</Typography>
                                </Grid> : null : null
                        }
                        <Grid item mt={3} textAlign="center">
                        {noti ? null : <Typography color="error" paddingBottom={2}>Chưa nhập đủ thông tin</Typography>}
                            <Button variant="contained" color="success" onClick={onBtnGui}>Thêm</Button>&nbsp;&nbsp;
                            <Button variant="contained" color="error" onClick={() => closeProps()}>Hủy bỏ</Button>
                        </Grid>
                    </Grid>

                </Box>
            </Modal>
            {/* Thông báo thành công or thất bại */}
            <Snackbar open={success} autoHideDuration={6000} onClose={() => { setSuccess(false) }}>
                <Alert severity="success" sx={{ width: '100%' }}>
                    {alertMessage}
                </Alert>
            </Snackbar>
            <Snackbar open={fail} autoHideDuration={6000} onClose={() => setFail(false)}>
                <Alert onClose={handleClose} severity="error" sx={{ width: '100%' }}>
                    {alertMessage}
                </Alert>
            </Snackbar>
        </Grid>
    )

}
export default ModalAdd;