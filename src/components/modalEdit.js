import { Grid, Typography, Button, Alert, Snackbar, Modal, Box, Select, MenuItem } from '@mui/material';
import { useEffect, useState } from 'react';
import { fetchAPI } from './FetchAPI';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: "400px",
    height: "600px",
    overflow: 'scroll',
    bgcolor: 'background.paper',
    border: '2px solid #000',
    p: 4,
};

function ModalEdit({ openProps, closeProps, idEdit }) {
    const [drinkList, setDrinkList] = useState([])
    const [alertMessage, setAlertMessage] = useState("")
    const [success, setSuccess] = useState(false);
    const [fail, setFail] = useState(false);
    const [trangThai, setTrangThai] = useState()

    const updateButtonModal = () => {
        updateByIDAPI(idEdit.id)
    }

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    const updateByIDAPI = (paramId) => {
        let body = {
            method: 'PUT',
            body: JSON.stringify({
                trangThai: trangThai
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        };
        if (JSON.parse(body.body)) {
            fetchAPI("http://42.115.221.44:8080/devcamp-pizza365/orders/" + paramId, body)
                .then((data) => {
                    setSuccess(true)
                    setAlertMessage("Update thành công");
                    closeProps()
                })
                .catch((err) => {
                    setFail(true);
                    setAlertMessage("Update thất bại")
                })
        }
    }


    useEffect(() => {
        fetchAPI("http://42.115.221.44:8080/devcamp-pizza365/drinks")
            .then((data) => {
                setDrinkList(data);
            })

    }, [idEdit])
    return (
        <Grid>
            <Modal open={openProps} aria-labelledby="modal-modal-title" aria-describedby="modal-modal-description">
                <Box sx={style}>
                    <Typography variant="h5" sx={{ pb: 2 }}>
                        Sửa user
                    </Typography>
                    <Grid sm={12}>
                        <Grid item>
                            <Typography htmlFor="fullname">Tên: {idEdit.hoTen}</Typography>
                        </Grid>
                        <Grid item mt={3}>
                            <Typography htmlFor="email">Email: {idEdit.email}</Typography>
                        </Grid>
                        <Grid item mt={3}>
                            <Typography htmlFor="dien-thoai">Số điện thoại: {idEdit.soDienThoai}</Typography>
                        </Grid>
                        <Grid item mt={3}>
                            <Typography htmlFor="dia-chi">Địa chỉ: {idEdit.diaChi}</Typography>
                        </Grid>
                        <Grid item mt={3}>
                            <Typography htmlFor="voucher">Mã giảm giá: {idEdit.idVourcher} ({idEdit.giamGia})</Typography>
                        </Grid>
                        <Grid item mt={3} mb={3}>
                            <Typography htmlFor="message">Lời nhắn: {idEdit.loiNhan}</Typography>
                        </Grid>
                        <Grid item mt={3} mb={3}>
                            <Typography htmlFor="message">Combo: {idEdit.kichCo}</Typography>
                            <Grid>
                                <Typography htmlFor="message" paddingTop={2}>Đường kính: {idEdit.duongKinh}cm</Typography>
                                <Typography htmlFor="message" paddingTop={2}>Sườn nướng: {idEdit.suon}</Typography>
                                <Typography htmlFor="message" paddingTop={2}>Salad: {idEdit.salad}g</Typography>
                                <Typography htmlFor="message" paddingTop={2}>Số lượng nước: {idEdit.soLuongNuoc}</Typography>
                                <Typography variant='h6' htmlFor="message" paddingTop={2}>Đơn giá: {numberWithCommas(idEdit.thanhTien)} VNĐ</Typography>
                            </Grid>
                        </Grid>
                        <Grid item mt={3} mb={3}>
                            <Typography htmlFor="message">Loại nước uống: {
                                drinkList.map((vDrink, index) => {
                                    return idEdit.idLoaiNuocUong === vDrink.maNuocUong ? vDrink.tenNuocUong : vDrink.idLoaiNuocUong
                                })
                            }</Typography>
                        </Grid>
                        <Grid item mt={3} mb={3}>
                            <Typography htmlFor="message">Loại Pizza: {idEdit.loaiPizza}</Typography>
                        </Grid>
                        <Grid item mt={3} mb={3}>
                            <Typography htmlFor="message">Trạng thái</Typography>
                            <Select label="status" defaultValue={idEdit.trangThai} fullWidth onChange={(event) => setTrangThai(event.target.value)}>
                                <MenuItem value="open">Open</MenuItem>
                                <MenuItem value="cancel">Cancel</MenuItem>
                                <MenuItem value="confirmed">Confirmed</MenuItem>
                            </Select>
                        </Grid>
                        <Grid>
                            {idEdit.giamGia ? <Typography htmlFor="message">Giảm giá: {numberWithCommas(idEdit.giamGia)} VNĐ ( {idEdit.giamGia / idEdit.thanhTien * 100} %)</Typography> : null}
                            <Typography variant='h6' htmlFor="message" padding={2}>Thành tiền: {numberWithCommas(idEdit.thanhTien - idEdit.giamGia)} VNĐ</Typography>
                        </Grid>
                    </Grid>
                    <Button variant="contained" color="success" onClick={updateButtonModal}>Update</Button>&nbsp;&nbsp;
                    <Button variant="contained" color="error" onClick={() => closeProps()}>Hủy bỏ</Button>
                </Box>
            </Modal>
            {/* Thông báo thành công or thất bại */}
            <Snackbar open={success} autoHideDuration={6000} onClose={() => { setSuccess(false) }}>
                <Alert severity="success" sx={{ width: '100%' }}>
                    {alertMessage}
                </Alert>
            </Snackbar>
            <Snackbar open={fail} autoHideDuration={6000} onClose={() => setFail(false)}>
                <Alert severity="error" sx={{ width: '100%' }}>
                    {alertMessage}
                </Alert>
            </Snackbar>
        </Grid>
    )

}
export default ModalEdit;