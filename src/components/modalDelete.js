import { Grid, Typography, Button, Alert, Snackbar, Modal, Box } from '@mui/material';
import { useEffect, useState } from 'react';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: "auto",
    height: "auto",
    bgcolor: 'background.paper',
    border: '2px solid #000',
    p: 4,
};

function ModalDelete({ openProps, closeProps, idDelete }) {
    const [alertMessage, setAlertMessage] = useState("")
    const [success, setSuccess] = useState(false);
    const [fail, setFail] = useState(false);
    const [idDel, setIdDel] = useState(idDelete)

    const deleteByAPI = (paramId) => {
        let body = {
            method: 'DELETE',
        };

        fetch("http://42.115.221.44:8080/devcamp-pizza365/orders/" + paramId, body)
            .then((data) => {
                setAlertMessage("Delete thành công");
                setSuccess(true);
                closeProps()
            })
            .catch((err) => {
                console.log(err)
                setFail(true);
                setAlertMessage("Delete thất bại")
            })
    }

    const deleteButtonModal = () => {
        deleteByAPI(idDelete);
        setSuccess(true);
    }


    useEffect(() => {
        setIdDel(idDelete)
    }, [idDelete])
    return (
        <Grid>
            <Modal open={openProps} aria-labelledby="modal-modal-title" aria-describedby="modal-modal-description">
                <Box sx={style}>
                    <Typography variant="h5" sx={{ pb: 2 }}>
                        Xóa user id: {idDel}
                    </Typography>
                    <Button variant="contained" color="error" onClick={deleteButtonModal}>Delete</Button>&nbsp;&nbsp;
                    <Button variant="contained" onClick={() => { closeProps() }}>Hủy bỏ</Button>
                </Box>
            </Modal>

            {/* Thông báo thành công or thất bại */}
            <Snackbar open={success} autoHideDuration={6000} onClose={() => { setSuccess(false) }}>
                <Alert severity="success" sx={{ width: '100%' }}>
                    {alertMessage}
                </Alert>
            </Snackbar>
            <Snackbar open={fail} autoHideDuration={6000} onClose={() => setFail(false)}>
                <Alert severity="error" sx={{ width: '100%' }}>
                    {alertMessage}
                </Alert>
            </Snackbar>
        </Grid>
    )

}
export default ModalDelete;